const tmi = require('tmi.js');
const child_process = require('child_process');

const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));

let command_queue = [];
let lock = false;

// Define configuration options
const opts = {
  identity: {
    username: 'icohmxy',
    password: 'oauth:thmwgkncdrfdxket0npq2q85qmu227',
  },
  channels: [
	  'icohmxy'
  ]
};

// Try to find Neverputt
out = child_process.spawnSync('xdotool', ['search', '--class', 'neverputt'])
PID = out.stdout.toString('utf8').trim()
if (out.stdout.toString('utf8') == '') {
	console.log('Cannot find Neverputt. Bye!')
	process.exit(1)
} else {
	console.log(`Neverputt found with PID: ${PID}`)
}

// Create a client with our options
const client = new tmi.client(opts);

// Register our event handlers (defined below)
client.on('message', onMessageHandler);
client.on('connected', onConnectedHandler);

// Connect to Twitch:
client.connect();

// Called every time a message comes in
function onMessageHandler (target, context, msg, self) {
  if (self) { return; } // Ignore messages from the bot

  // Remove whitespace from chat message
  const command_name = msg.trim().toLocaleLowerCase();

  if (['l', 'r', 's', '+', '-'].includes(command_name)) {
	  console.log(`Appended command: ${command_name}`);
	  command_queue.push(command_name);
  }

}

async function send_key(key, duration=0) {
	if (duration > 0) {
		let cmd = child_process.spawn('xdotool', ['keydown', '--window', PID, key]);

		cmd.on('close', (code) => {
			sleep(duration).then(() => {
				let cmd2 = child_process.spawn('xdotool', ['keyup', '--window', PID, key]);
				cmd2.on('close', (code) => {
					console.log(`Command closed with code ${code}. Unlock.`)
					lock = false;
				});
			});
		});

	} else {

		let cmd = child_process.spawn('xdotool', ['key', '--window', PID, key]);

		cmd.on('close', (code) => {
			console.log(`Command closed with code ${code}. Unlock.`)
			lock = false;
		});
	}
}

// Function called when the "dice" command is issued
function rollDice () {
  const sides = 6;
  return Math.floor(Math.random() * sides) + 1;
}

// Called every time the bot connects to Twitch chat
function onConnectedHandler (addr, port) {
  console.log(`* Connected to ${addr}:${port}`);
}

function run_command(command) {
  if (command === 'l') {
	send_key('Right', 500);
  } else if (command === 'r') {
	send_key('Left', 500);
  } else if (command === '+') {
	send_key('Down', 250);
  } else if (command === '-') {
	send_key('Up', 250);
  } else if (command === 's') {
	send_key('Enter');
  }
}

setInterval(function() {
	if (!lock && command_queue.length > 0) {
		lock = true;
		let command = command_queue.shift();
		console.log(`Running command ${command}. Locked.`);
		run_command(command);
	}
}, 100);
